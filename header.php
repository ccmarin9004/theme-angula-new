<!DOCTYPE html>
<html ng-app="ng-App">
    <head>
        <meta charset="utf-8">
        <title>Theme Angular</title>    
    </head>
    <header>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.min.js"></script>
        <script type="text/javascript" src="https://code.angularjs.org/1.6.5/angular-sanitize.js"></script>
        <script type="text/javascript" src="js/controlador.js"></script>
        <script type="text/javascript" src="js/servicio.js"></script>
        <?php wp_head(); ?>
    </header>
  <body ng-controller="controlador">
