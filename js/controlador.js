angular.module('ng-App', ['ngSanitize'])
.controller('controlador', ['$scope', 'service', function($scope, service) {
    service.getPosts()
    .then(function(response){
      $scope.posts = response;
    }, function(error){
      console.log(error);
    });
}]);
