angular.module('ng-App')
.factory('service', ['$http', '$q', function($http, $q) {
    return {
      getPosts: function() {
        var deferred = $q.defer();
        $http.get('http://localhost/wp-json/wp/v2/posts')
        .then(function(response){
           deferred.resolve(response.data);
        }, function(error){
          deferred.reject(error);
        });
        return deferred.promise;
      }
    }
}]);

// {
//    "post":[
//       {
//          "id":,
//          "date":"2017",
//          "guid":{
//             "rendered":"#"
//          },
//          "modified":"2017-20-12",
//          "modified_gmt":"",
//          "slug":"animals",
//          "type":"post",
//          "link":"#",
//          "title":{
//             "rendered":"Animales Lorem"
//          },
//          "content":{
//             "rendered":"Lorem"
//          }
//       }
//    ]
// }
